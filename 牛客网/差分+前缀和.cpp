#include <bits/stdc++.h>
using namespace std;

// https://ac.nowcoder.com/acm/problem/16649
// 二维前缀和公式：s[x2, y2] - s[x2, y1 - 1] -s[x1 - 1, y2] + s[x1 - 1, y2 - 1]
void set1(vector<int> &diff, int l, int r, int val)
{
    diff[l] -= val;
    diff[r + 1] += val;
}

int main()
{
    int L, M;
    cin >> L >> M;
    vector<int> diff(L + 2, 0); // 到这里都是模板
    while (M--)
    {
        int l, r;
        cin >> l >> r;
        set1(diff, l, r, 1);
    }
    for (int i = 1; i <= L; i++)
        diff[i] += diff[i - 1];
    int ans = 0;
    for (int i = 0; i <= L; i++)
    {
        if (diff[i] == 0) // 0是有树的情况
            ans++;
    }
    cout << ans;
    return 0;
}